(function(){
	'use strict';
	angular.module('kakifoodApp')
	.controller('CreateCtrl', CreateCtrl);

	/////////////////////////////////////

	CreateCtrl.$inject = ['$scope', '$rootScope', '$modal', 'userLocation', 'UserLocation'];
	function CreateCtrl($scope, $rootScope, $modal, userLocation, UserLocation){

		$rootScope.showFoods = true;

		$scope.hasUserLocation = false;
		$scope.openShareFoodModal = openShareFoodModal;

		init();

		function init(){
			$scope.hasUserLocation = true;	
		}

		function openShareFoodModal(){
			UserLocation.getUserLocation().then(function(data){
				
				var modalInstance = $modal.open({
					templateUrl: 'shareFoodModal.html',
					controller: 'ShareFoodCtrl',
					size: 'sm',
					resolve: {
						controllerscope:function(){
							return $scope;
						},
						userLocation: function(){
							return data;
						}
					}
				});

				modalInstance.result.then(function (food) {
					console.log(food);
					$rootScope.$broadcast('addFoodMarker', [food]);
					// $scope.openUploadImageModal(shop);
				}, function () {
					// Shop.showAddShopModal = false;
				});
			});
		}

		// open add form modal
		function openAddShopModal() {

			
		};
	}

	angular.module('kakifoodApp')
	.controller('ShareFoodCtrl', function ($scope, $rootScope, controllerscope, $modalInstance, userLocation, Food) {
		
		var loc = [ userLocation.longitude, userLocation.latitude ];

		$scope.food = {
			name: '',
			description: '',
			total: 1,
			available: 1,
			price: 0,
			loc: loc

		};
		$scope.shareYourFood = shareYourFood;

		function shareYourFood(){
			$scope.onCreating = true;
			$scope.food.available = $scope.food.total;

			Food.createFood($scope.food).success(function(food){

				$scope.onCreating = false;
				console.log(food);
				$modalInstance.close(food);
			});
		}
	});

})();