(function(){
	'use strict';
	angular.module('kakifoodApp')
	.config(config);

	/////////////////////////////////////

	function config($stateProvider){
		$stateProvider
			.state('create', {
				url: '/create',
				templateUrl: 'app/create/create.html',
				controller: 'CreateCtrl',
				resolve: {
					userLocation: function(UserLocation){
						return UserLocation.getUserLocation();
					}
				}
			});
	}
})();