(function(){
	'use strict';

	angular.module('kakifoodApp')
	.controller('MainCtrl', MainCtrl);

	MainCtrl.$inject = ['$scope', '$rootScope', '$state', '$window', 'Auth'];
	function MainCtrl($scope, $rootScope, $state, $window, Auth){

		$scope.searchFood = searchFood;
		$scope.shareFood = shareFood;

		function searchFood(){
			if(Auth.isLoggedIn()){
				$state.go('search');
			}
		}

		function shareFood(){
			if(Auth.isLoggedIn()){
				$state.go('create');
			}else{
				// $state.go('create');
				$window.location.href = '/auth/facebook';
			}
		}

		// when content loaded
		$rootScope.$on('$viewContentLoaded', function(event, toState, toParams, fromState, fromParams){ 
			$('.full-bg').css( { 'height': $(window).height() } );
		});
	}

})();

