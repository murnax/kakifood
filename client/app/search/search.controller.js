(function(){
	'use strict';
	angular.module('kakifoodApp')
	.controller('SearchCtrl', SearchCtrl);

	/////////////////////////////////////

	SearchCtrl.$inject = ['$scope', '$rootScope', '$modal', 'UserLocation', 'Food'];
	function SearchCtrl($scope, $rootScope, $modal, UserLocation, Food){

		$rootScope.showFoods = true;

		$scope.foods = [];

		init();

		function init(){
			UserLocation.getUserLocation().then(function(data){
				
				// user location
				var location = {
					lat: data.latitude, 
					lng: data.longitude
				};

				// Food.getNearByFoods(location).success(function(foods){
				// 	$scope.foods = foods;
				// 	$rootScope.$broadcast('showFoods', foods);
				// });
			});
		}

		$scope.$watch(function(){ return Food.selectedFood }, function(newVal){
			
			if(newVal){
				var modalInstance = $modal.open({
					templateUrl: 'reserveFoodModal.html',
					controller: 'ReserveFoodCtrl',
					size: 'sm',
					resolve: {
						controllerscope:function(){
							return $scope;
						}
					}
				});	

				modalInstance.result.then(function (shop) {
					// $scope.openUploadImageModal(shop);
				}, function () {
					// Shop.showAddShopModal = false;
				});
			}
		});
	}

	angular.module('kakifoodApp')
	.controller('ReserveFoodCtrl', function ($scope, controllerscope, $modalInstance, Food) {
		
		// $scope.reserveFood()
		// var loc = [ userLocation.longitude, userLocation.latitude ];

		// $scope.food = {
		// 	name: '',
		// 	description: '',
		// 	total: 1,
		// 	available: 1,
		// 	price: 0,
		// 	loc: loc

		// };
		// $scope.shareYourFood = shareYourFood;

		// function shareYourFood(){
		// 	$scope.onCreating = true;
		// 	$scope.food.available = $scope.food.total;

		// 	Food.createFood($scope.food).success(function(food){

		// 		$scope.onCreating = false;
		// 		$modalInstance.close(food);
		// 	});
		// }
	});
})();