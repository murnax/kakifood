(function(){
	'use strict';
	angular.module('kakifoodApp')
		.config(config);

	/////////////////////////////////////

	function config($stateProvider){
		$stateProvider
			.state('search', {
				url: '/search',
				templateUrl: 'app/search/search.html',
				controller: 'SearchCtrl'
			});
	}
})();