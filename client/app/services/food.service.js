(function(){
	'use strict';
	angular.module('kakifoodApp')
		.factory('Food', Food);

	/////////////////////////////////////

	Food.$inject = ['$http'];
	function Food($http){
		var baseUrl = '/api/food';

		return {
			getNearByFoods: getNearByFoods,
			get: get,
			createFood: createFood
		}

		/////////////////////////////////////

		function createFood(food){
			return $http.post(baseUrl + '', food);
		}

		function getNearByFoods(location){
			return $http.get(baseUrl + '?lat=' + location.lat + '&lng=' + location.lng + '&distance=300');
		}

		function get(id){
			return $http.get(baseUrl + '?id=' + id);
		}
	}
})();