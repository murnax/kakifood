(function(){
	'use strict';
	angular.module('kakifoodApp')
		.factory('UserLocation', UserLocation);

	/////////////////////////////////////

	UserLocation.$inject = ['$q'];
	function UserLocation($q){
		var userLocation = null;

		var service = {
			location: null,
			getUserLocation: getUserLocation,
			setUserLocation: setUserLocation
		}
		return service;

		/////////////////////////////////////

		function getUserLocation(){
			var deferred  = $q.defer();
			if(userLocation === null){
				if (navigator.geolocation) {
					navigator.geolocation.getCurrentPosition(function(location){
						
						deferred.resolve({ latitude: location.coords.latitude, longitude: location.coords.longitude });
					}, showError);
				}else {
					$scope.error = "Geolocation is not supported by this browser.";
				}	
			}else{
				deferred.resolve(userLocation);
			}

			return deferred.promise;
		}

		function setUserLocation(location){
			userLocation = location;
			service.location = location;
		}

		function showError(error){
			console.log(error);
		}
	}
})();