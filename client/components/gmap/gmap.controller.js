'use strict';

angular.module('kakifoodApp')
.controller('GMapCtrl', GMapCtrl);

//////////////////////////////////////////////////////////////////

GMapCtrl.$inject = ['$scope', '$rootScope', '$http', '$window', '$location', '$timeout', '$state', 'uiGmapGoogleMapApi', 'Food', 'UserLocation'];

function GMapCtrl($scope, $rootScope, $http, $window, $location, $timeout, $state, uiGmapGoogleMapApi, Food, UserLocation) {
	uiGmapGoogleMapApi.then(googleMapApiLoaded);

	var bangkokLatLong = { latitude: 13.7539800, longitude: 100.5014400 };
	var centralRama9 = { latitude: 13.7576994, longitude: 100.5652238 };
	var europe = { latitude: 46.76758746952729, longitude: 23.60080003738403 };

	var userLocation = { latitude: 13.757879162647567, longitude: 100.56592653876191 };

	var userCenter = userLocation;

	$scope.shopIcon = {
		size: {
			width: 60,
			height: 112
		},
		scaledSize: {
			width: 30,
			height: 56
		},
		anchor: {
			x: 15,
			y: 28
		},
		url: 'assets/icon/pin-orange.svg'
	};
	$scope.userIcon = {
		size: {
			width: 96,
			height: 96
		},
		scaledSize: {
			width: 48,
			height: 48
		},
		anchor: {
			x: 24,
			y: 48
		},
		url: 'assets/icon/icon-man.svg'
	}

	$scope.map = {
		control: {},
		center: userCenter,
		draggable: false,
		markers: [],
		zoom: 8,
		pan: true,
		events: {},
		options: {
			styles: [
			{
				stylers: [
				{
					"hue": "#000"
				},
				{
					"saturation": 0
				},
				{
					"lightness": -66
				},
				{
					"visibility": "on"
				}
				]
			},{
				featureType: "road",
				elementType: "geometry",
				stylers: [
				{ lightness: 100 },
				{ visibility: "simplified" }
				]
			},{
				featureType: "road",
				elementType: "labels",
				stylers: [
				{ visibility: "off" }
				]
			},{
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    }
			]
		}
	}

	$scope.circles = [
            {
                id: 1,
                center: $rootScope.userCenter,
                radius: 500000,
                stroke: {
                    color: '#dfc52d',
                    weight: 2,
                    opacity: 1
                },
                fill: {
                    color: '#dfc52d',
                    opacity: 0.05
                },
                geodesic: true, // optional: defaults to false
                draggable: true, // optional: defaults to false
                clickable: true, // optional: defaults to true
                editable: true, // optional: defaults to false
                visible: true, // optional: defaults to true
                control: {}
            }
        ];

	$scope.setMapCenter = setMapCenter;

	$scope.getLocation = getLocation;

	$scope.showPosition = showPosition;

	$scope.$on('showAllShops', showAllShops);

	$scope.$on('setMapCenter', function(event, params){
		console.log(params);
		$scope.setMapCenter(params.location);
	});

	$scope.$on('removeShopMarker', function(event, params){
		// console.log($scope.map.markers, params);
		var shopId = params[0];
		
		console.log($scope.map.markers);
		_.each($scope.map.markers, function(marker, index){
			
			if(marker.id === shopId){
				console.log('found', index);
				$timeout(function(){
					$scope.map.markers.splice(0, 1);
					console.log($scope.map.markers);
				},3000);
			}
		});
	});

	$scope.$on('addFoodMarker', function(event, params){
		console.log(params);
		var food = params[0];
		var marker = {};
		marker.id = food._id;
		marker.coords = {
			longitude: food.loc[0],
			latitude: food.loc[1]
		};

		marker.events = {
			click: function(marker, eventName, args){

				var foodId = marker.key;
				console.log(foodId);
				Food.selectedFood = foodId;
							
			}
		}
		marker.icon = $scope.shopIcon; //'assets/images/shop-marker-icon.png';

		$scope.map.markers.push(marker);

		$rootScope.userCurrentLocation = $rootScope.userOriginalLocation;
		
		$scope.setMapCenter($rootScope.userOriginalLocation);
	});

	$scope.$on('setUserLocation', setUserLocation);

	$scope.$on('clearNewShopMarker', function(){
		$scope.map.markers = [];
	});

	$scope.$on('setShopLocation', function(){
		Shop.showAddShopModal = true;
	});

	//////////////////////////////////////////////////////////////////

	function setMapCenter(location){
		$scope.map.center = {
			latitude: location.latitude,
			longitude: location.longitude
		}
		$scope.map.zoom = 17;
	}

	function setUserLocation(event, params){
		console.log('set user location');
		$scope.draggableUserMarker = params[0].draggable;
		// remove all markers from GMap
		$scope.map.markers = [];
		// call get l ocation function
		$scope.getLocation();
	}

	function getLocation(){
		console.log('get location');
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition($scope.showPosition, $scope.showError);
		}
		else {
			$scope.error = "Geolocation is not supported by this browser.";
		}
	}

	function showPosition(position){
		console.log('show user\'s current position');

		var data = {
			markerWidth: 48,
			markerHeiht: 48
		};
		
		// override postion from geolocation with Central Rama 9
		var lat = 13.7576994;
		var lng = 100.5652238;


		var marker = {};
		marker.id = $scope.map.markers.length + 1;
		marker.coords = userCenter;

		$rootScope.userCurrentLocation = userCenter;
		$rootScope.userOriginalLocation = userCenter;
		console.log(userOriginalLocation);

		marker.type = 'user';

		// user marker
		marker.icon = $scope.userIcon;
		marker.options = {};

		if($scope.draggableUserMarker){
			marker.options.draggable = true;
			marker.events = {
				dragend: function (marker, eventName, args) {
					$rootScope.userCurrentLocation = {
						latitude: marker.getPosition().lat(),
						longitude: marker.getPosition().lng()
					}
				}
			}
		}
		$scope.setMapCenter(userCenter);


		$scope.map.markers.push(marker);

		$scope.$apply();

		// set flag on the service to let controller know that shop already has location
		$rootScope.hasUserLocation = true;
		// set shop's location
		Shop.newShopLocation = {
			latitude: lat,
			longitude: lng
		};

		AlertMessage.message = '';
	}

	function showAllShops(events, shops){

		//$scope.getLocation();
		$scope.map.zoom = 10;

		_.each(shops, function(shop){

			var marker = {};
			marker.id = shop._id;
			marker.coords = {
				longitude: shop.loc[0],
				latitude: shop.loc[1]
			};
			
			marker.events = {
				click: function(marker, eventName, args){
					var shopId = marker.key;
					Shop.getShop(shopId).success(function(shop){
						Shop.selectedShop = shop;
						setMapCenter({latitude: shop.loc[1], longitude: shop.loc[0]});
					});
				}
			}
			marker.icon = $scope.shopIcon; //'assets/images/shop-marker-icon.png';

			$scope.map.markers.push(marker);	
		});
	}

	function googleMapApiLoaded(maps){

		$rootScope.googleMapApiLoaded = true;

		$(function(){
			$('.angular-google-map-container').hide().css({
				'height': $(window).height()
			}).fadeIn();
		});

		$timeout(function(){
			var directionsService = $scope.map.control.getGMap();
		}, 1000);
		
		UserLocation.getUserLocation().then(function(data){
			// userCenter = {
			// 	latitude: data.coords.latitude, 
			// 	longitude: data.coords.longitude
			// }
			userCenter = data;
			showFoods(userCenter);

			setMapCenter(userCenter);

			var marker = {};
			marker.id = $scope.map.markers.length + 1;
			marker.coords = userCenter;

			$rootScope.userCurrentLocation = userCenter;
			$rootScope.userOriginalLocation = userCenter;
			marker.type = 'user';

			// user marker
			marker.icon = $scope.userIcon;
			marker.options = {};

			if(true || $scope.draggableUserMarker){
				marker.options.draggable = true;
				marker.events = {
					dragend: function (marker, eventName, args) {
						
						var location = {
							latitude: marker.getPosition().lat(),
							longitude: marker.getPosition().lng()
						}
						UserLocation.setUserLocation(location);

						showFoods(location);
						
						// $rootScope.userCurrentLocation = {
						// 	latitude: marker.getPosition().lat(),
						// 	longitude: marker.getPosition().lng()
						// }
					}
				}
			}
			$scope.setMapCenter(userCenter);
			$scope.map.markers.push(marker);			
		});
}

function showFoods(location){
	if($rootScope.showFoods){
		Food.getNearByFoods({ lat: location.latitude, lng: location.longitude }).success(function(foods){

			$scope.map.markers.splice(1);

			_.each(foods, function(food){
				var marker = {};
				marker.id = food._id;
				marker.coords = {
					longitude: food.loc[0],
					latitude: food.loc[1]
				};

				marker.events = {
					click: function(marker, eventName, args){

						var foodId = marker.key;
						console.log(foodId);
						Food.selectedFood = foodId;
							// Food.getFood(foodId).success(function(food){
							// 	console.log(food);
							// });
							// Shop.getShop(shopId).success(function(shop){
							// 	Shop.selectedShop = shop;
							// 	setMapCenter({latitude: shop.loc[1], longitude: shop.loc[0]});
							// });
		}
	}
					marker.icon = $scope.shopIcon; //'assets/images/shop-marker-icon.png';

					$scope.map.markers.push(marker);
				});
		});	
	}
}
}