'use strict';

angular.module('kakifoodApp')
  .controller('NavbarCtrl', function ($scope, $location, Auth) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    }];

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.logout = function() {
      Auth.logout();
      $location.path('/');
    };

    $scope.getUserProfileUrl = function(){
      var user = $scope.getCurrentUser();
      var profileImage;
      if(user.provider == 'twitter'){
        profileImage = user.twitter.profile_image_url;  
      }else if(user.provider == 'facebook'){
        profileImage = 'https://graph.facebook.com/' + user.facebook.id + '/picture?type=square';
      }
      return profileImage; 
    }

    $scope.isActive = function(route) {
      return route === $location.path();
    };
  });