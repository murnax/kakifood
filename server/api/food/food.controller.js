'use strict';
var _ = require('lodash');
// var multiparty = require('multiparty');
// var util = require('util');
// var uuid = require('uuid');
var fs = require('fs');
var Food = require('./food.model');
var User = require('../user/user.model');

exports.getNear = function(req, res){
	var maxDistance = (req.query['distance'] !== undefined) ? req.query['distance'] : 1000;

	var coords = [];
	if(req.query['lng'] !== undefined && req.query['lat'] !== undefined){
		coords[0] = req.query['lng'];
		coords[1] = req.query['lat'];
	}
	console.log(maxDistance, coords);
	Food.find({
		loc: {
			$near:  {  
				$geometry: {  
					type: "Point",  
					coordinates: coords
				}, 
				$maxDistance: maxDistance
			}	
		}
	}).limit(10).exec(function(err, chefs){
		if(err){
			res.send(500, err);
		}
		return res.json(200, chefs);
	});

}
exports.findFoodById = function(req,res)
{
	var id = req.param.id;
	if(id!=undefined)
	{

		Food.findOne(id,function(err,item)
	    {
	    	if (!err) 
	    	{

	    		 res.send(item);

	    	}else{
	    		res.send({'error':'An error has occurred'});
	    	}
		  

	     } );
	}else{

		res.send('id is required');
	}
	

}

exports.updateFood = function(req,res)
{

	var id = req.param.id;
	var food = req.body;


	if(id!=undefined)
	{

		Food.update(id,food,function(err,result)
	    {

	    	if(!err)
	    	{
                console.log('Success: ' + JSON.stringify(result[0]));
                res.send(result[0]);

	    	}
	    	else{
	    		res.send({'error':'An error has occurred'});
	    	}

	     } );
	}else{

		res.send('id is required');
	}

}

exports.addFood = function(req,res)
{

	var food = req.body;


	Food.create(food,function(err,result)
	{

		if(!err)
		{
			res.send(result);
			// console.log('Success: ' + JSON.stringify(result[0]));
   //              res.send(result[0]);

		}else{
			res.send({'error':'An error has occurred'});
		}

	}

	);

}

