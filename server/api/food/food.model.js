'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var FoodSchema = new Schema({
	name: String,
	description: String,
	total: Number,
	available: Number,
	price: Number,
	loc: {
		type: [Number],
		index: '2dsphere'
	},
	creator_id: String,
	created_date: { 
		type: Date,
		default: Date.now()
	}
});

FoodSchema.index({ loc: '2dsphere' });
module.exports = mongoose.model('Food', FoodSchema);