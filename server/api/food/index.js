'use strict';

var express = require('express');
var controller = require('./food.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.getNear);
router.post('/',controller.addFood);
 router.post('/:id',controller.updateFood);
router.get('/:id',controller.findFoodById)

module.exports = router;
