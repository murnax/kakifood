'use strict';

var express = require('express');
var controller = require('./order.controller');
var config = require('../../config/environment');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.getAll);
router.post('/', controller.orderFood);

module.exports = router;