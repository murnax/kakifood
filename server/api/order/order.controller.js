'use strict'
var _ = require('lodash');
ar fs = require('fs');

var Order = require('./Order.model');

exports.findAll = function(req,res)
{
	Order.find().toArray(function(err,items)
	{

		if (!err) {

			res.send(items);

		}else
		{
			res.send({'error':'An error has occurred'});

		}
	});


}

exports.findById = function(req,res)
{

	var id = req.param.id;
	if(id!=undefined)
	{

		Order.findOne(id,function(err,item)
	    {
	    	if (!err) 
	    	{

	    		 res.send(item);

	    	}else{
	    		res.send({'error':'An error has occurred'});
	    	}
		  

	     } );
	}else{

		res.send('id is required');
	}

}

exports.addOrder = function(req,res)
{
	var order = req.body;


	Order.create(order,function(err,result)
	{

		if(!err)
		{

			console.log('Success: ' + JSON.stringify(result[0]));
                res.send(result[0]);

		}else{
			res.send({'error':'An error has occurred'});
		}

	}

	);

}