'use strict';

var mongoose = require('mongoose')
	Schema = mongoose.Schema;

var OrderSchema = new Schema({
	buyer_id: String,
	food_id: String,
	amount: Number,
	total: Number,
	created_date: {
		type: Date,
		default: Date.now()
	}
});

module.exports = mongoose.model('Order', OrderSchema);