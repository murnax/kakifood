'use strict';

// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  DOMAIN:           'http://localhost:9000',
  SESSION_SECRET:   'foodsharing-secret',

  FACEBOOK_ID:      '1450066815301918',
  FACEBOOK_SECRET:  'ebe1c7432669b8947636a0d64f6e2fcc',

  // Control debug level for modules using visionmedia/debug
  DEBUG: ''
};
